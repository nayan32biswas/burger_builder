import React, { Component } from 'react'
// from BurgerBuilder
import Button from '../../UI/Button/Button'

class OrderSummary extends Component {
    componentWillUpdate() {
        console.log("ComponentWillUpdate")
    }
    render() {
        const ingredientsSummary = Object.keys(this.props.ingredients).map((igKey) => {
            return (
                <li key={igKey}>
                    <span style={{ textTransform: 'capitalize' }}>{igKey}</span>: {this.props.ingredients[igKey]}
                </li>);
        });
        return (
            <div>
                <h3>Your Order</h3>
                <p>A Delicious burger with the following ingredients:</p>
                <ul>{ingredientsSummary}</ul>
                <p>Continue to Checkout</p>
                <Button btnType="Danger" clicked={this.props.purchasingCancel}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchasingContinue}>CONTINUE</Button>
            </div>
        );
    }
}

export default OrderSummary;