import React, { Component } from 'react'
// from App
import classes from './Layout.css'
import Toolbar from '../../components/Navigation/Toolbar/Toolbar'
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer'

class Layout extends Component {
    state = {
        showSideDrqwer: false
    }
    sideDrawerCloseHandler = () => {
        this.setState({ showSideDrqwer: false })
    }
    sideDrawerToggleHandler = () => {
        this.setState ( (prevState) => {
            return {showSideDrqwer: !prevState.showSideDrqwer}
        })
    }
    render() {
        return (
            <div>
                <Toolbar sideDrawerToggleHandler={this.sideDrawerToggleHandler} />
                <SideDrawer
                    open={this.state.showSideDrqwer}
                    closed={this.sideDrawerCloseHandler}
                />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </div>
        )
    }
}

export default Layout;