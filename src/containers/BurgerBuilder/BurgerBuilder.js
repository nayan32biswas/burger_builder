import React, { Component } from 'react'
// from App
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'

const ingredientPrice = {
    salad: 0.5,
    bacon: 0.7,
    cheese: 0.4,
    meat: 1.3
}

class BurgerBuilder extends Component {
    state = {
        ingredients: {
            salad: 1,
            bacon: 1,
            cheese: 1,
            meat: 1
        },
        burgerPrice: 2.90 + 1.1,
        purchasable: false,
        purchasing: false
    }

    purchaseHandler = (updateCopy) => {
        const sum = Object.keys(updateCopy).map((igKey) => {
            return updateCopy[igKey];
        }).reduce((sum, el) => {
            return sum + el;
        }, 0)
        this.setState({ purchasable: sum <= 0 });
    }
    addIngredientHandler = (type) => {
        const updateCopy = { ...this.state.ingredients };
        updateCopy[type]++;
        const price = this.state.burgerPrice + ingredientPrice[type];
        this.setState({ burgerPrice: price, ingredients: updateCopy });
        this.purchaseHandler(updateCopy);

    }
    removeIngredientHandler = (type) => {
        const updateCopy = { ...this.state.ingredients };
        updateCopy[type]--;
        const price = (this.state.burgerPrice - ingredientPrice[type]);
        this.setState({ burgerPrice: price, ingredients: updateCopy });
        this.purchaseHandler(updateCopy);
    }
    purchasingHandler = () => {
        this.setState({ purchasing: true });
    }
    purchasingCloseHandler = () => {
        this.setState({ purchasing: false });
    }

    purchasingCancelHandler = () => {
        this.setState({ purchasing: false })
    }
    purchasingContinueHandler = () => {
        alert("Continue Handler");
    }
    render() {
        const disabledInfo = { ...this.state.ingredients }
        for (let key in disabledInfo) {
            disabledInfo[key] = (disabledInfo[key] <= 0)
        }
        return (
            <div>
                <Modal show={this.state.purchasing} orderCancel={this.purchasingCloseHandler}>
                    <OrderSummary
                        ingredients={this.state.ingredients}
                        purchasingCancel={this.purchasingCancelHandler}
                        purchasingContinue={this.purchasingContinueHandler}
                    />
                </Modal>
                <Burger ingredients={this.state.ingredients} />
                <BuildControls
                    controls={this.state.ingredients}
                    addIngredient={this.addIngredientHandler}
                    removeIngredient={this.removeIngredientHandler}
                    price={this.state.burgerPrice}
                    disabled={disabledInfo}
                    purchasable={this.state.purchasable}
                    ordered={this.purchasingHandler}
                />
            </div>
        );
    }
}

export default BurgerBuilder;